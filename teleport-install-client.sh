#!/bin/bash
# curl -O https://get.gravitational.com/teleport-v10.0.2-linux-amd64-bin.tar.gz
tar -xzf teleport-v10.0.2-linux-amd64-bin.tar.gz
rm -rf teleport-v10.0.2-linux-amd64-bin.tar.gz
cd teleport
./install
cd ../
rm -rf teleport/
