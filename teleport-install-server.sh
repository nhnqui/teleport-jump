#!/bin/bash
# curl -O http://jump.cybertron.vn/teleport/teleport-v7.3.0-linux-amd64-bin.tar.gz
# tar -xzf teleport-v7.3.0-linux-amd64-bin.tar.gz
# rm -rf teleport-v7.3.0-linux-amd64-bin.tar.gz
curl -O https://get.gravitational.com/teleport-v10.0.2-linux-amd64-bin.tar.gz
tar -xzf teleport-v10.0.2-linux-amd64-bin.tar.gz
rm -rf teleport-v10.0.2-linux-amd64-bin.tar.gz
cd teleport
./install
cd ../
rm -rf teleport/
username=$1
token=$2
#teleport start --roles=node --auth-server=jump.cybertron.vn:3025 --token=$token --labels hostname=[1m:"hostname"],env=$username --insecure
touch /etc/systemd/system/teleport-node.service
echo "[Unit]
Description=Teleport Node Service
After=network.target

[Service]
Type=simple
Restart=on-failure
ExecStart=/usr/local/bin/teleport start --roles=node --auth-server=jump.cybertron.vn:3025 --token=$token --labels hostname=[1m:"hostname"],env=$username --pid-file=/run/teleport.pid
ExecReload=/bin/kill -HUP $MAINPID
PIDFile=/run/teleport.pid

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/teleport-node.service
systemctl daemon-reload
systemctl enable teleport-node.service
systemctl start teleport-node.service
systemctl status teleport-node.service
