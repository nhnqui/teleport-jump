# Install teleport on server
### Test connection
- dig jump.cybertron.vn
- telnet jump.cybertron.vn 80
- telnet jump.cybertron.vn 3025
### Get token
- curl http://jump.cybertron.vn/node_token/
### Install teleport node
- download install-teleport-server.sh
- chmod +x install-teleport-server.sh
- ./install-teleport-server.sh username token

# Use teleport WEB on client
- https://jump.cybertron.vn/web/login

# Install, use teleport CLI on client
### Ubuntu install package
- download install-teleport-client.sh
- chmod +x install-teleport-client.sh
- ./install-teleport-client.sh
### Login teleport
- tsh login --proxy jump.cybertron.vn  --user username
- enter password
- enter otp
### List server
- tsh ls
### SSH
- tsh ssh username@servername
### SCP
- tsh scp ./text username@servername:/test/

### SSH to server with login
tsh ssh --proxy=jump.cybertron.vn --user={username} {username}@{node-name}

### MacOS, Windows install CLI
- https://goteleport.com/docs/installation/
